#!/usr/bin/env sh
if [ ! -f app.js ]; then
    npx express-generator -v nunjucks -f
fi 
npm install
npm start
